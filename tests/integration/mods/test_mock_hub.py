import inspect
from unittest.mock import sentinel

import pytest


def test_mock_hub_dereference_errors(mock_hub):
    with pytest.raises(AttributeError, match="has no attribute 'nosub'"):
        mock_hub.nosub.nomodule.nofunc()

    with pytest.raises(AttributeError, match="has no attribute 'nomodule'"):
        mock_hub.mods.nomodule.nofunc()

    with pytest.raises(AttributeError, match="has no attribute 'nofunc'"):
        mock_hub.mods.testing.nofunc()


def test_mock_hub_function_enforcement(mock_hub):
    with pytest.raises(TypeError, match="missing a required argument: 'param'"):
        mock_hub.mods.testing.echo()


def test_mock_hub_return_value(mock_hub):
    mock_hub.mods.testing.echo.return_value = sentinel.myreturn
    assert mock_hub.mods.testing.echo("param") is sentinel.myreturn


@pytest.mark.asyncio
async def test_async_echo(hub, mock_hub):
    val = "foo"
    ret = await hub.mods.testing.async_echo(val)
    assert ret == val

    mock_hub.mods.testing.async_echo.return_value = val
    assert await mock_hub.mods.testing.async_echo(val + "change") == val


def test_signature(hub, mock_hub):
    assert inspect.signature(hub.pop.sub.add) == inspect.signature(mock_hub.pop.sub.add)
